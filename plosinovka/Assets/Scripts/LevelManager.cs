using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;

public class LevelManager : MonoBehaviour {
    public static LevelManager instance;
    
    public Transform respawnPoint;
    public GameObject playerPrefab;

    public CinemachineVirtualCameraBase cam;

    [Header("Currency")]
    public int currency = 0;
    public Text currencyUI;

    private void Awake() {
        instance = this;
    }
    public void Respawn () {
        GameObject player = Instantiate(playerPrefab, respawnPoint.position, Quaternion.identity);
        cam.Follow = player.transform;
        currency = 0; //kdyz respawn, dat currency na 0
        currencyUI.text = "~" + currency; //kdyz respawn, refreshnout currency text
        rickSound.Stop();
    }

    public void IncreaseCurrency(int amount) {
        currency += amount;
        currencyUI.text = "~" + currency;
    }

    public static LevelManager Instance; //static = muzu to zavolat odkudkoliv
    public AudioSource coinSound; //zvuk penez
    public AudioSource screamSound; //zvuk kriku
    public AudioSource rickSound; //rickroll

    void Start() {
        Instance = this;     
    }
}
