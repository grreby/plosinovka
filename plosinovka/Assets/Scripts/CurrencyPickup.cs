using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrencyPickup : MonoBehaviour {
    public int worth = 100;

    private void Start() {

    }

    void OnTriggerEnter2D(Collider2D collision) {
        if (collision.CompareTag("Player")) {
            Destroy(gameObject);
            LevelManager.Instance.IncreaseCurrency(worth);

            LevelManager.Instance.coinSound.Play();
        }
    }
}