using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlacksmithSound : MonoBehaviour {

    public AudioSource audioSource;
    public Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        anim.Play("Kovar");
        audioSource.Play();
    }

}